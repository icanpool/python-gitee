# Namespace

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | namespace id | [optional] 
**type** | **str** | namespace 类型，企业：Enterprise，组织：Group，用户：null | [optional] 
**name** | **str** | namespace 名称 | [optional] 
**path** | **str** | namespace 路径 | [optional] 
**html_url** | **str** | namespace 地址 | [optional] 
**parent** | [**NamespaceMini**](NamespaceMini.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

