# GroupFollowers

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**_self** | [**UserBasic**](UserBasic.md) |  | [optional] 
**followed_at** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

