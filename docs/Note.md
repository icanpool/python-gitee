# Note

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**body** | **str** |  | [optional] 
**body_html** | **str** |  | [optional] 
**user** | [**UserBasic**](UserBasic.md) |  | [optional] 
**source** | **str** |  | [optional] 
**target** | **str** |  | [optional] 
**created_at** | **datetime** |  | [optional] 
**updated_at** | **datetime** |  | [optional] 
**in_reply_to_id** | **str** |  | [optional] 
**in_reply_to_user** | [**UserBasic**](UserBasic.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

