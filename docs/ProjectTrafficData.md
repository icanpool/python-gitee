# ProjectTrafficData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**counts** | [**list[ProjectTrafficDataDesc]**](ProjectTrafficDataDesc.md) | 每天的访问量数据集 | [optional] 
**summary** | [**ProjectTrafficDataSummary**](ProjectTrafficDataSummary.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

