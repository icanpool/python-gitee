# WeekReport

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**content** | **str** |  | [optional] 
**content_html** | **str** |  | [optional] 
**year** | **str** |  | [optional] 
**month** | **str** |  | [optional] 
**week_index** | **str** |  | [optional] 
**week_begin** | **str** |  | [optional] 
**week_end** | **str** |  | [optional] 
**created_at** | **datetime** | 创建时间 | [optional] 
**updated_at** | **datetime** | 更新时间 | [optional] 
**user** | [**UserMini**](UserMini.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

