# ProtectionRule

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**project_id** | **str** |  | [optional] 
**wildcard** | **str** |  | [optional] 
**pushers** | **list[str]** |  | [optional] 
**mergers** | **list[str]** |  | [optional] 
**contexts** | **list[str]** | 检查项列表 | [optional] 
**strict** | **bool** | 是否严格检查 | [optional] 
**mode** | **str** | 模式 standard: 标准模式, review: 评审模式 | [optional] 
**escapse_protect_branch_list** | **list** | 不受规则影响的常规分支列表，以英文逗号分隔，形如：[&#x27;a&#x27;, &#x27;b&#x27;] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

