# PostV5ReposOwnerRepoCommitsActions

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | **str** | 文件操作：&#x60;create&#x60;, &#x60;delete&#x60;, &#x60;move&#x60;, &#x60;update&#x60;, &#x60;chmod&#x60; | 
**path** | **str** | 文件路径 | 
**previous_path** | **str** | 原文件路径，文件重命名 &#x60;move&#x60; 时使用 | [optional] 
**content** | **str** | 文件内容 | [optional] 
**encoding** | **str** | 文件内容编码 &#x60;text&#x60; 或者 &#x60;base64&#x60; | [optional] 
**last_commit_id** | **str** | 文件最近一次提交的 SHA | [optional] 
**execute_filemode** | **bool** | 是否添加文件可执行标志 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

