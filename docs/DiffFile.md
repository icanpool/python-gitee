# DiffFile

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**sha** | **str** |  | [optional] 
**filename** | **str** | 文件路径 | [optional] 
**status** | **str** | 文件状态 | [optional] 
**additions** | **int** | 新增行数 | [optional] 
**deletions** | **int** | 删除行数 | [optional] 
**changes** | **int** | 变更行数 | [optional] 
**blob_url** | **str** | blob 链接 | [optional] 
**raw_url** | **str** | raw 链接 | [optional] 
**content_url** | **str** | content 链接 | [optional] 
**patch** | **str** | patch | [optional] 
**truncated** | **bool** | patch 内容是否被截断 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

