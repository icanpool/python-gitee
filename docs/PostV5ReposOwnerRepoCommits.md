# PostV5ReposOwnerRepoCommits

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**branch** | **str** | 变更的目标分支名。创建新分支时需提供 &#x60;start_branch&#x60; 参数 | 
**message** | **str** | 提交信息 | 
**actions** | [**list[PostV5ReposOwnerRepoCommitsActions]**](PostV5ReposOwnerRepoCommitsActions.md) |  | 
**start_branch** | **str** | 分支起地点。新建分支时使用，更新分支时可选 | [optional] 
**author** | [**PostV5ReposOwnerRepoCommitsAuthor**](PostV5ReposOwnerRepoCommitsAuthor.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

