# IssueState

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务状态 ID | [optional] 
**title** | **str** | 任务状态的名称 | [optional] 
**color** | **str** | 任务状态的颜色 | [optional] 
**icon** | **str** | 任务状态的 Icon | [optional] 
**command** | **str** | 任务状态的 指令 | [optional] 
**serial** | **str** | 任务状态的 排序 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

