# WildcardSettingBody

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access_token** | **str** | 用户授权码 | [optional] 
**new_wildcard** | **str** | 新分支/通配符(为空不修改) | [optional] 
**pusher** | **str** | 可推送代码成员。developer：仓库管理员和开发者；admin：仓库管理员；none：禁止任何人合并; 用户：个人的地址 path（多个用户用 &#x27;;&#x27; 隔开） | [default to 'admin']
**merger** | **str** | 可合并 Pull Request 成员。developer：仓库管理员和开发者；admin：仓库管理员；none：禁止任何人合并; 用户：个人的地址 path（多个用户用 &#x27;;&#x27; 隔开） | [default to 'admin']
**mode** | **str** | 模式。standard: 标准模式, review: 评审模式 | [default to 'standard']
**escapse_protect_branch_list** | **list[str]** | 不受规则影响的常规分支列表，以英文逗号分隔，形如：[&#x27;a&#x27;, &#x27;b&#x27;] | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

