# Issue

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**url** | **str** |  | [optional] 
**repository_url** | **str** |  | [optional] 
**labels_url** | **str** |  | [optional] 
**comments_url** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**parent_url** | **str** |  | [optional] 
**number** | **str** | 唯一标识 | [optional] 
**parent_id** | **int** | 上级 id | [optional] 
**depth** | **int** | 所在层级 | [optional] 
**state** | **str** | 状态 | [optional] 
**title** | **str** | 标题 | [optional] 
**body** | **str** | 描述 | [optional] 
**body_html** | **str** | 描述 html 格式 | [optional] 
**user** | [**UserBasic**](UserBasic.md) |  | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**assignee** | [**UserBasic**](UserBasic.md) |  | [optional] 
**collaborators** | [**UserBasic**](UserBasic.md) |  | [optional] 
**repository** | [**Project**](Project.md) |  | [optional] 
**milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**created_at** | **datetime** | 创建时间 | [optional] 
**updated_at** | **datetime** | 更新时间 | [optional] 
**plan_started_at** | **datetime** | 计划开始时间 | [optional] 
**deadline** | **datetime** | 结束时间 | [optional] 
**finished_at** | **datetime** | 完成时间 | [optional] 
**scheduled_time** | **int** | 预计工期 | [optional] 
**comments** | **int** | 评论数量 | [optional] 
**priority** | **int** | 优先级(0: 不指定 1: 不重要 2: 次要 3: 主要 4: 严重) | [optional] 
**issue_type_detail** | [**IssueType**](IssueType.md) |  | [optional] 
**program** | [**ProgramBasic**](ProgramBasic.md) |  | [optional] 
**security_hole** | **bool** | 是否为私有issue | [optional] 
**issue_state_detail** | [**IssueState**](IssueState.md) |  | [optional] 
**branch** | **str** | 关联分支 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

