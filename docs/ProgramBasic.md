# ProgramBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 项目id | [optional] 
**name** | **str** | 项目名称 | [optional] 
**description** | **str** | 项目描述 | [optional] 
**assignee** | [**UserBasic**](UserBasic.md) |  | [optional] 
**author** | [**UserBasic**](UserBasic.md) |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

