# Compare

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**base_commit** | [**RepoCommit**](RepoCommit.md) |  | [optional] 
**merge_base_commit** | [**RepoCommit**](RepoCommit.md) |  | [optional] 
**commits** | [**list[RepoCommit]**](RepoCommit.md) | commits 数量限制在 100 以内 | [optional] 
**files** | [**list[DiffFile]**](DiffFile.md) | 文件列表 | [optional] 
**truncated** | **bool** | 文件列表是否被截断 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

