# IssueType

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 任务类型 ID | [optional] 
**title** | **str** | 任务类型的名称 | [optional] 
**template** | **str** | 任务类型模板 | [optional] 
**ident** | **str** | 唯一标识符 | [optional] 
**color** | **str** | 颜色 | [optional] 
**is_system** | **bool** | 是否系统默认类型 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

