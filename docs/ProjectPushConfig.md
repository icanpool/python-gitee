# ProjectPushConfig

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**restrict_push_own_commit** | **str** |  | [optional] 
**restrict_author_email_suffix** | **str** |  | [optional] 
**author_email_suffix** | **str** |  | [optional] 
**restrict_commit_message** | **str** |  | [optional] 
**commit_message_regex** | **str** |  | [optional] 
**restrict_file_size** | **str** |  | [optional] 
**max_file_size** | **str** |  | [optional] 
**except_manager** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

