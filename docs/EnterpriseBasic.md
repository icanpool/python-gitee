# EnterpriseBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** | 企业ID | [optional] 
**path** | **str** | 企业命名空间 | [optional] 
**name** | **str** | 企业名称 | [optional] 
**url** | **str** | 企业地址 | [optional] 
**avatar_url** | **str** | 企业头像地址 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

