# RepoCommitWithFiles

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**url** | **str** |  | [optional] 
**sha** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**comments_url** | **str** |  | [optional] 
**commit** | **str** |  | [optional] 
**author** | [**UserBasic**](UserBasic.md) |  | [optional] 
**committer** | [**UserBasic**](UserBasic.md) |  | [optional] 
**parents** | **str** |  | [optional] 
**stats** | **str** |  | [optional] 
**files** | [**list[DiffFile]**](DiffFile.md) | 文件列表 | [optional] 
**truncated** | **bool** | 文件列表是否被截断 | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

