# ProjectBasic

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **int** |  | [optional] 
**full_name** | **str** |  | [optional] 
**human_name** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**namespace** | [**NamespaceMini**](NamespaceMini.md) |  | [optional] 
**path** | **str** | 仓库路径 | [optional] 
**name** | **str** | 仓库名称 | [optional] 
**owner** | [**UserBasic**](UserBasic.md) |  | [optional] 
**assigner** | [**UserBasic**](UserBasic.md) |  | [optional] 
**description** | **str** | 仓库描述 | [optional] 
**private** | **bool** | 是否私有 | [optional] 
**public** | **bool** | 是否公开 | [optional] 
**internal** | **str** | 是否内部开源 | [optional] 
**fork** | **bool** | 是否是fork仓库 | [optional] 
**html_url** | **str** |  | [optional] 
**ssh_url** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

