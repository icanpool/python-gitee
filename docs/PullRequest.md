# PullRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**url** | **str** |  | [optional] 
**html_url** | **str** |  | [optional] 
**diff_url** | **str** |  | [optional] 
**patch_url** | **str** |  | [optional] 
**issue_url** | **str** |  | [optional] 
**commits_url** | **str** |  | [optional] 
**review_comments_url** | **str** |  | [optional] 
**review_comment_url** | **str** |  | [optional] 
**comments_url** | **str** |  | [optional] 
**number** | **str** |  | [optional] 
**state** | **str** |  | [optional] 
**assignees_number** | **str** |  | [optional] 
**testers_number** | **str** |  | [optional] 
**assignees** | **list[str]** |  | [optional] 
**testers** | **list[str]** |  | [optional] 
**api_reviewers_number** | **str** |  | [optional] 
**api_reviewers** | **list[str]** |  | [optional] 
**milestone** | [**Milestone**](Milestone.md) |  | [optional] 
**labels** | [**Label**](Label.md) |  | [optional] 
**locked** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**updated_at** | **str** |  | [optional] 
**closed_at** | **str** |  | [optional] 
**draft** | **str** |  | [optional] 
**merged_at** | **str** |  | [optional] 
**mergeable** | **str** |  | [optional] 
**can_merge_check** | **str** |  | [optional] 
**links** | **str** |  | [optional] 
**user** | **str** |  | [optional] 
**ref_pull_requests** | [**list[RefPullRequest]**](RefPullRequest.md) |  | [optional] 
**title** | **str** |  | [optional] 
**body** | **str** |  | [optional] 
**body_html** | **str** |  | [optional] 
**head** | **str** |  | [optional] 
**base** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

