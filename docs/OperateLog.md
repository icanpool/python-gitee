# OperateLog

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **str** |  | [optional] 
**icon** | **str** |  | [optional] 
**user** | **str** |  | [optional] 
**target** | **str** |  | [optional] 
**content** | **str** |  | [optional] 
**link_target** | **str** |  | [optional] 
**created_at** | **str** |  | [optional] 
**action_type** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

